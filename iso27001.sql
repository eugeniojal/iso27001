-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 08-01-2020 a las 23:20:57
-- Versión del servidor: 10.1.38-MariaDB
-- Versión de PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `iso27001`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nivel3`
--

DROP TABLE IF EXISTS `nivel3`;
CREATE TABLE `nivel3` (
  `id` int(10) NOT NULL,
  `titulo` varchar(350) COLLATE utf8_spanish_ci NOT NULL,
  `nivel2` varchar(350) COLLATE utf8_spanish_ci NOT NULL,
  `nivel1` varchar(350) COLLATE utf8_spanish_ci NOT NULL,
  `estatus` int(11) NOT NULL,
  `documento` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `ucarga` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `nivel3`
--

INSERT INTO `nivel3` (`id`, `titulo`, `nivel2`, `nivel1`, `estatus`, `documento`, `ucarga`) VALUES
(1, 'A.5.1.1  Políticas de la seguridad de de la información  ', '1', '1', 0, 'Hoja Membretada CEG Farfan.docx', 0),
(2, 'A.5.1.2 	Revisión de las políticas de seguridad de la información ', '1', '1', 2, '', 0),
(3, 'A.6.1.1 	Funciones y \r\nresponsabilidades de la seguridad de la información \r\n', '2', '2', 0, 'CV - Eugencio J. Antón L..docx', 0),
(4, 'A.6.1.2 	Segregación de tareas ', '2', '2', 0, '', 0),
(5, 'A.6.1.3 	Contacto con las autoridades ', '2', '2', 0, '', 0),
(6, 'A.6.1.4 	Contacto con grupos especiales de interés ', '2', '2', 0, '', 0),
(7, 'A.6.1.5 	Seguridad de la información en la gestión del proyecto ', '2', '2', 0, '', 0),
(8, 'A.6.2.1 	Política de los equipos móviles ', '3', '2', 0, '', 0),
(9, 'A.6.2.2 	Trabajo a distancia ', '3', '2', 0, '', 0),
(10, 'A.7.1.1 	Filtración ', '4', '3', 0, '', 0),
(11, 'A.7.1.2 	Términos y condiciones del empleo ', '4', '3', 0, '', 0),
(12, 'A.7.2.1 	Responsabilidades de la Gerencia ', '5', '3', 0, '', 0),
(13, 'A.7.2.2 	Concientización, educación y capacitación sobre seguridad de la \r\ninformación \r\n', '5', '3', 0, '', 0),
(14, 'A.7.2.3 	Procesos disciplinarios ', '5', '3', 0, '', 0),
(15, 'A.7.3.1 	Término o cambio de responsabilidades de empleo ', '6', '3', 0, '', 0),
(16, 'A.8.1.1 	Inventario de activos ', '7', '4', 0, '', 0),
(17, 'A.8.1.2 	Propiedad de los activos ', '7', '4', 0, '', 0),
(18, 'A.8.1.3 	Uso aceptable de los activos ', '7', '4', 0, '', 0),
(19, 'A.8.1.4 	Retorno de los activos ', '7', '4', 0, '', 0),
(20, 'A.8.2.1 	Clasificación de la información ', '8', '5', 0, '', 0),
(21, 'A.8.2.2 	Etiquetado de la información ', '8', '5', 0, '', 0),
(22, 'A.8.2.3 	Manejo de los activos ', '8', '5', 0, '', 0),
(23, 'A.8.3.1 	Gestión de medios de comunicación removibles ', '9', '5', 0, '', 0),
(24, 'A.8.3.2 	Disposición de los medios comunicación ', '9', '5', 0, '', 0),
(25, 'A.8.3.3 	Transferencia física de los medios de comunicación ', '9', '5', 0, '', 0),
(26, 'A.9.1.1 	Política de control de acceso ', '', '6', 0, '', 0),
(27, 'A.9.1.2 	Acceso a las redes y a los servicios de las redes ', '', '6', 0, '', 0),
(28, 'A.9.2.1 	Registro y des registro del usuario ', '', '6', 0, '', 0),
(29, 'A.9.2.2 	Provisión de acceso al usuario ', '', '6', 0, '', 0),
(30, 'A.9.2.3 	Gestión de los derechos de acceso privilegiado ', '', '6', 0, '', 0),
(31, 'A.9.2.4. 	Gestión de información de autenticación secreta de usuarios ', '', '6', 0, '', 0),
(32, 'A.9.2.5 	Verificación de los derechos de acceso de los usuarios ', '', '6', 0, '', 0),
(33, 'A.9.2.6 	Retiro o ajuste de los derechos de acceso ', '', '6', 0, '', 0),
(34, 'A.9.3.1 	Uso de información secreta de autenticación ', '', '6', 0, '', 0),
(35, 'A.9.4.1 	Restricción del acceso a la información ', '', '6', 0, '', 0),
(36, 'A.9.4.2 	Procedimiento seguro de logeo ', '', '6', 0, '', 0),
(37, 'A.9.4.3 	Sistema de gestión de la clave ', '', '6', 0, '', 0),
(38, 'A.9.4.4 	Uso de programas \r\nutilitarios de privilegio \r\n', '', '6', 0, '', 0),
(39, 'A.9.4.5 	Control del acceso para programar el código fuente ', '', '6', 0, '', 0),
(40, 'A.10.1.1 	Política del uso de controles criptográficos ', '', '7', 0, '', 0),
(41, 'A.10.1.2 	Gestión de las claves ', '', '7', 0, '', 0),
(42, 'A.11.1.1 	Perímetro de seguridad \r\nfísica \r\n', '', '8', 0, '', 0),
(43, 'A.11.1.2 	Controles físicos de los ingresos ', '', '8', 0, '', 0),
(44, 'A.11.1.3 	Seguridad de las oficinas, salas e instalaciones ', '', '8', 0, '', 0),
(45, 'A.11.1.4 	Protección contra las amenazas externas y medioambientales ', '', '8', 0, '', 0),
(46, 'A.11.1.5 	Trabajo en áreas seguras ', '', '8', 0, '', 0),
(47, 'A.11.1.6 	Distribución de las zonas de carga ', '', '8', 0, '', 0),
(48, 'A.11.2.1 	Ubicación y protección de los equipos ', '', '8', 0, '', 0),
(49, 'A.11.2.2 	Servicios públicos de soporte ', '', '8', 0, '', 0),
(50, 'A.11.2.3 	Seguridad en el cableado ', '', '8', 0, '', 0),
(51, 'A.11.2.4 	Mantenimiento de los equipos ', '', '8', 0, '', 0),
(52, 'A.11.2.5 	Retiro de los activos ', '', '8', 0, '', 0),
(53, 'A.11.2.6 	Seguridad de los equipos y bienes fuera de las instalaciones ', '', '8', 0, '', 0),
(54, 'A.11.2.7 	Disposición o reúso seguro de los equipos ', '', '8', 0, '', 0),
(55, 'A.11.2.8 	Usuario de equipo abandonado ', '', '8', 0, '', 0),
(56, 'A.11.2.9 	Política de escritorio y pantallas limpias ', '', '8', 0, '', 0),
(57, 'A.12.1.1 	Documentación de los procedimientos operacionales ', '', '', 0, '', 0),
(58, 'A.12.1.2 	Cambios en la gerencia ', '', '', 0, '', 0),
(59, 'A.12.1.3 	Gestión de la capacidad ', '', '', 0, '', 0),
(60, 'A.12.1.4 	Separación de ambientes de desarrollo, prueba y de operaciones ', '', '', 0, '', 0),
(61, 'A.12.2.1 	Controles contra el malware ', '', '', 0, '', 0),
(62, 'A.12.3.1  	Backup de la información ', '', '', 0, '', 0),
(63, 'A.12.4.1 	Eventos de logeo  ', '', '', 0, '', 0),
(64, 'A.12.4.2 	Protección de la información del logeo ', '', '', 0, '', 0),
(65, 'A.12.4.3 	Logeo del administrador y operador ', '', '', 0, '', 0),
(66, 'A.12.4.4 	Sincronización de los relojes ', '', '', 0, '', 0),
(67, 'A.12.5.1 	Instalación del software en los sistemas operacionales ', '', '', 0, '', 0),
(68, 'A.12.6.1 	Gestión de las \r\nvulnerabilidades técnicas \r\n', '', '', 0, '', 0),
(69, 'A.12.6.2 	Restricciones en la instalación de software ', '', '', 0, '', 0),
(70, 'A.12.7.1 	Controles de la auditoría sobre los sistemas de \r\ninformación \r\n', '', '', 0, '', 0),
(71, 'A.13.1.1 	Controles en las redes ', '', '', 0, '', 0),
(72, 'A.13.1.2  	Seguridad de los servicios de las redes ', '', '', 0, '', 0),
(73, 'A.13.1.3 	Segregación en las redes ', '', '', 0, '', 0),
(74, 'A.13.2.1 	Políticas y procedimientos de la transferencia de la información ', '', '', 0, '', 0),
(75, 'A.13.2.2 	Acuerdos sobre la transferencia de la información ', '', '', 0, '', 0),
(76, 'A.13.2.3 	Mensajes electrónicos ', '', '', 0, '', 0),
(77, 'A.13.2.4 	Confidencialidad o acuerdos no divulgados ', '', '', 0, '', 0),
(78, 'A.14.1.1 	Análisis y especificaciones de los requisitos de la seguridad de la información ', '', '', 0, '', 0),
(79, 'A.14.1.2 	Seguridad de los servicios de aplicación en las redes públicas ', '', '', 0, '', 0),
(80, 'A.14.1.3 	Protección de las transacciones de los servicios de aplicación ', '', '', 0, '', 0),
(81, 'A.14.2.1 	Política del programa de desarrollo seguro ', '', '', 0, '', 0),
(82, 'A.12.2.2 	Procedimiento de control de los cambios de sistemas ', '', '', 0, '', 0),
(83, 'A.14.2.3 	Revisión técnica de las aplicaciones luego de los cambios de la plataforma operacional ', '', '', 0, '', 0),
(84, 'A.14.2.4 	Restricciones a los cambios de los paquetes de software ', '', '', 0, '', 0),
(85, 'A.14.2.5 	Principios del sistema de seguridad para la ingeniería ', '', '', 0, '', 0),
(86, 'A.14.2.6 	Ambiente seguro del programa de desarrollo ', '', '', 0, '', 0),
(87, 'A.14.2.7 	Programa de desarrollo subcontratado  ', '', '', 0, '', 0),
(88, 'A.14.2.8 	Revisión de la seguridad del sistema ', '', '', 0, '', 0),
(89, 'A.14.2.9 	Revisión de la aceptación del sistema ', '', '', 0, '', 0),
(90, 'A.14.3.1  	Protección de los datos de prueba ', '', '', 0, '', 0),
(91, 'A.15.1.1 	Política de seguridad de la información sobre las ', '', '', 0, '', 0),
(92, 'A.15.1.2 	Consideración de la seguridad en los acuerdos con los proveedores ', '', '', 0, '', 0),
(93, 'A.15.1.3 	Cadena de suministro de \r\ntecnología de la información y comunicación \r\n', '', '', 0, '', 0),
(94, 'A.15.2.1 	Monitoreo y revisión del \r\nservicio de los proveedores \r\n', '', '', 0, '', 0),
(95, 'A.15.2.2 	Cambios en la gestión del \r\nservicio de los proveedores \r\n', '', '', 0, '', 0),
(96, 'A.16.1.1 	Responsabilidades y procedimientos ', '', '', 0, '', 0),
(97, 'A.16.1.2 	Reporte de los eventos de seguridad de la información ', '', '', 0, '', 0),
(98, 'A.16.1.3 	Reporte de las debilidades de la seguridad de la información ', '', '', 0, '', 0),
(99, 'A.16.1.4 	Evaluación y decisión sobre los eventos de seguridad de la información ', '', '', 0, '', 0),
(100, 'A.16.1.5 	Respuesta a los incidentes de seguridad de la información ', '', '', 0, '', 0),
(101, 'A.16.1.6 	Aprendizaje de los incidentes de seguridad de la información ', '', '', 0, '', 0),
(102, 'A.16.1.7 	Recolección de evidencia ', '', '', 0, '', 0),
(103, 'A.17.1.1 	Continuidad de los planes de seguridad de la información ', '', '', 0, '', 0),
(104, 'A.17.1.2 	Implementación de la continuidad de la seguridad de la información ', '', '', 0, '', 0),
(105, 'A.17.1.3 	Verificación, revisión y evaluación de la continuidad de la seguridad de la información ', '', '', 0, '', 0),
(106, 'A.17.2.1 	Disponibilidad de instalaciones de procesamiento de la información ', '', '', 0, '', 0),
(107, 'A.18.1.1 	Identificación de la ley aplicable y de los requisitos contractuales ', '', '', 0, '', 0),
(108, 'A.18.1.2 	Derechos de propiedad intelectuales ', '', '', 0, '', 0),
(109, 'A.18.1.3 	Protección de los registros ', '', '', 0, '', 0),
(110, 'A.18.1.4 	Privacidad y protección de la información que permite identificar a las personas ', '', '', 0, '', 0),
(111, 'A.18.1.5 	Regulación de los controles criptográficos ', '', '', 0, '', 0),
(112, 'A.18.2.1 	Revisión independiente de la seguridad de la información ', '', '', 0, '', 0),
(113, 'A.18.2.2 	Cumplimiento de las políticas y normas de seguridad de la información ', '', '', 0, '', 0),
(114, 'A.18.2.3 	Revisión del cumplimiento técnico ', '', '', 0, '', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE `usuarios` (
  `id` int(10) NOT NULL,
  `usuario` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `nombre` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `pass` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `tipo` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `usuario`, `nombre`, `pass`, `tipo`) VALUES
(1, 'usuario', 'usuario', '12345', 0);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `nivel3`
--
ALTER TABLE `nivel3`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `nivel3`
--
ALTER TABLE `nivel3`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=115;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
