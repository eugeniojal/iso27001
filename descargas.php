<?php 
include('default/header.php');
?>
  <div class="row">
            <!-- Pending Requests Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-secondary shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Documento para Descargar</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800">Norma ISO/IEC 27001:2013</div>
                    </div>
                    <div class="col-auto">
                    	<a href="27001.pdf" >
                       <i class="fas fa-download"></i></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
                    <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-secondary shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Documento para Descargar </div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800">Norma ISO/IEC 27001:2013</div>
                    </div>
                    <div class="col-auto">
                    	<a href="27002.pdf" >
                     <i class="fas fa-download"></i></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
 </div>

 <?php
include('default/footer.php');





 ?>