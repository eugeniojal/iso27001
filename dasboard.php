<?php 

include('default/header.php');
include('default/conexion.php');

$query1 = mysqli_query($enlace,"SELECT * FROM nivel3 where estatus = 1");
$pendientes = mysqli_num_rows($query1); 

$query2 = mysqli_query($enlace,"SELECT * FROM nivel3 where estatus = 2");
$completas = mysqli_num_rows($query2); 

$query3 = mysqli_query($enlace,"SELECT * FROM nivel3 where estatus = 0");
$inmcompletas = mysqli_num_rows($query3); 

$query4 = mysqli_query($enlace,"SELECT * FROM nivel3 where documento != '' ");
$documentos = mysqli_num_rows($query4); 
$query5 = mysqli_query($enlace,"SELECT * FROM nivel3 ");
$total = mysqli_num_rows($query5); 


$porcentaje = ($completas*100)/$total;

?>

   <div class="row">
            <!-- Pending Requests Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Tareas en proceso</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $pendientes; ?></div>
                    </div>
                    <div class="col-auto">
                       <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
                    <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Tareas en completadas</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $completas ?></div>
                    </div>
                    <div class="col-auto">
                       <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-danger shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Tareas en inmcompletas</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $inmcompletas ?></div>
                    </div>
                    <div class="col-auto">
                       <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
           <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-info shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Tareas con archivos</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $documentos ?></div>
                    </div>
                    <div class="col-auto">
                       <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
    </div>


              <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">BARRA DE COMPLETIUD</h6>
                </div>
                <div class="card-body">

                  <h4 class="small font-weight-bold">Norma ISO/IEC 27001 <span class="float-right"><?php echo round($porcentaje); ?>% Completo!</span></h4>
                  <div class="progress">
                    <div class="progress-bar bg-success" role="progressbar" style="width: <?php echo $porcentaje; ?>%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                </div>
              </div>


<?php

include('default/footer.php');



 ?>